\documentclass[10pt,a4paper,conference]{IEEEtran}
%\usepackage[margin=0.4in]{geometry}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{placeins}
\usepackage{multirow}
\usepackage{cite}
\usepackage{verbatim}
\usepackage{underscore}
\usepackage{colortbl}
\usepackage{amssymb}
\usepackage[table*]{xcolor}
\usepackage[hidelinks]{hyperref}

\xdefinecolor{gray95}{gray}{0.65}
\xdefinecolor{gray25}{gray}{0.8}

\title{Relationship between optimum population size and problem dimensions \\
	\large COS710 Assignment 4}
\author{Renette Ros 13007557}
\date{\today}

\begin{document}
	\maketitle
\begin{abstract}
	Particle Swarm Optimisation, Genetic Algorithms, Evolutionary Programs and Differential evolution are all stochastic, population-based search algorithms that can solve multidimensional optimization problems.  This research investigates the relationship between problem dimensions and the optimum population size for these algorithms on a set of 8 benchmark functions that include uni-modal, multi-modal and rotated functions. The conclusions reached is that the optimum population size is problem dependent and cannot be generalized for the amount of problem dimensions. 
\end{abstract}
\section{Introduction}
Particle Swarm Optimisation, Genetic Algorithms, Evolutionary Programs and Differential evolution are all stochastic, population-based search algorithms \cite{engelbrecht2007computational, storn1997differential, eberhart1995new} that can solve multidimensional optimisation problems. Each of these algorithms needs to set a population size which can influence the performance of the algorithm. 

This research will investigate whether a clear relationship exists between the number of problem dimensions and the optimum population size for these algorithms. 

The structure of this report is as follows: Section \ref{sec:back} will give some background information on these algorithms, Section \ref{sec:impl} describes the implementation details and Section \ref{sec:results} contains the results of the study. Section \ref{sec:concl} describes the conclusions reached. 

\section{Background} \label{sec:back}
\subsection{PSO}
Particle Swarm Optimisation (PSO) was developed by Kennedy and Eberhart \cite{eberhart1995new} in 1995. It differs from the other three algorithms in that it is not based on evolution, but on bird flocking. In the PSO particles fly through hyperspace towards their personal best and a neighbourhood best. Their is updated at every iteration and depends on their distance from these two best particles. This research uses the canonical PSO with an inertia weight component \cite{shi1998modified}. 

\subsection{GA}
The Genetic Algorithm (GA) is an evolutionary Algorithm based on natural selection. It was popularized by Holland \cite{holland1975adaptation}. In GAs candidate solutions are represented as Chromosomes. Selection (survival of the fittest) and crossover (reproduction) operators are applied to these individuals \cite{holland1975adaptation, engelbrecht2007computational}. It was originally created for binary problems but has been adapted for floating point problems. 

\subsection{EP}
Evolutionary Programming (EP), originating from Fogel's research \cite{fogel1962autonomous} is another algorithm that mimics natural evolutary processes. EPs differs from GAs because it focuses on behavioural models not genetic models \cite{engelbrecht2007computational}. The EP does not use recombination operators but only uses variation (mutation) and selection.  Selection in EP is based on relative fitness values - the current individual is compared to a number of random individuals to determine its score.

\subsection{DE}
Differential Evolution (DE) is an evolutionary algorithm that use distance  and direction information from the population to guide searches. The original DE, developed by Storn and Price \cite{price2006differential} was aimed at solving continuous valued problems. The DE algorithm uses a mutation operator to generate a trial vector. The trial vector is used within crossover to generate one offspring. The mutation step sizes are not sampled from a porbability distribution but are based on differences between individuals of the current population \cite{engelbrecht2007computational}. 

\section{Implementation} \label{sec:impl}
JMetal \cite{Durillo2011760} 5.1 was used for the GA and DE algorithms in this study. My own implementations of the PSO and EP was used. 
\subsection{Algorithms}
This research investigated the relationship between problem dimensions and swarm/population size for Particle Swarm Optimisation (PSO), a Genetic Algorithm (GA), Differential Evolution (DE) and an Evolutionary Program (EP)
All algorithms were run over a maximum of 5000 iterations (PSO) or generations (Evolutionary Aglorithms). 

\subsubsection{PSO}
A gbest PSO (star neighbourhood topology) was used. The PSO parameters were chosen to ensure convergence \cite{engelbrecht2007computational, trelea2003particle}:\\
\textbf{Inertia weight (w)} 0.789244\\
\textbf{Acceleration Coefficients (c1, c2)} 1.4296180

\subsubsection{GA}
The Genetic Algorithm (GA) implementation from JMetal 5.1 was used. 

\textbf{Crossover} The GA used Arithmetic crossover~ \cite{michalewicz2013genetic}, as shown in Equation \ref{eq:arithcross}, to create two children from two parents. A crossover probability of 90\% was used. The $\gamma$ value was chosen randomly for each crossover operation from $U(0,1)$. 
\begin{equation} \label{eq:arithcross}
\begin{split}
\tilde{x}_{1j}(t) = (1-\gamma)x_{1j}(t) + \gamma x_{2j}(t) \\
\tilde{x}_{2j}(t) = (1-\gamma)x_{2j}(t) + \gamma x_{1j}(t)
\end{split}
\end{equation} 

\textbf{Mutation} A Gaussian mutation operator was used that simply adds noise sampled from $N(0,1)$ to the value of a gene. The Gaussian mutation operator was chosen because of the assignment requirements. A mutation probability of $\frac{1}{d}$ was used where $d$ represents the problem dimensions. 

\textbf{Selection} The genetic algorithm uses tournament selection with a tournament size of 2 individuals. This choice was made based on the default GA selection operator in JMetal 5.1. 

\subsubsection{EP}
The Evolutionary Program (EP) has two parameters: Mutation and the size of the competition pool.

\textbf{Mutation} The EP uses Gaussian mutation that samples from the distribution $N(0,0.5)$. A deviation of 0.5 was chosen since literature suggests the value should be small \cite{engelbrecht2007computational}. The mutation probability was set to 80\% as mutation is the only means of introducing diversity. 

\textbf{Competition Pool} For every run the size of the competition pool, the pool that is used to calculate relative scores is set to one third of the population.

\textbf{Selection} The EP uses elitist selection: The new population is selected as the best individual from the combined original and offspring populations. 

The EP is non-adaptive so the strategy parameters stay constant during execution.   
\\
\subsubsection{DE}
For the Differential Evolution algorithm, all the default JMetal parameters, except population size,  were used.
\textbf{Recombination} A recombination probability of 50\% was used with the "rand/1/bin" variant and a scaling factor $\beta=0.5$. 
\textbf{\textbf{Selection}} The DE algorithm selects 3 random individuals to generate trial vectors. 
\\
\subsection{The Problems}
All problems were evaluated on 2, 10, 30 and 50 dimensions. The specific dimensions were chosen because the CEC2005 benchmark set \cite{suganthan2005problem} defines rotation matrices for only those specific dimensions. The chosen problems can be divided into 3 categories: Uni-modal functions, separable multi-modal functions and non-separable multi-modal functions. 
\\
\subsubsection{Uni-modal problems} Two  separable Uni-Modal functions were investigated, the Absolute and Spherical functions:

\textbf{Absolute value} 
\[\begin{split}
f_{abs}(x) = \sum_{i=1}^{n_x}\left|x_i\right| \\
x \in [-100, 100] \\
\end{split}\]

\textbf{Spherical Function}
\[\begin{split}
f_{sphe} = \sum_{i=1}^{n_x} x^2_i \\
x \in [-5.12, 5.12] \\  
\end{split}\]
\\
\subsubsection{Multi-modal Separable problems}
\hfill 

\textbf{Rastrigin Function}
\[\begin{split}
f_{ras}(x) = 10n + \sum_{i=1}^{n_x} (x_i^2 -10cos(2\pi x_i))\\
x \in [-5.12, 5.12] 
\end{split}\]

A rotated version of the Rastrigin, $f_{ras}^R$ fucntion was also ivnestigate. The function  was rotated with the rotation matrices defined in the CEC2005 \cite{suganthan2005problem} benchmark set for 2,10, 30 and 50 dimensions. 

\textbf{Michalewicz Function}
\[\begin{split}
	f_{mic}(x) = - \sum_{i=1}^d sin(x_i)(sin(\frac{ix_i^2}{\pi}))^{2m} \\
	x \in [0, \pi]~and~m=10 \\
\end{split}\]
\\
\subsubsection{Multi-modal non-separable problems}
\hfill 

\textbf{Ackley Function}
\[\begin{split}
f_{ack}(x) = -20 e^{-0.2 \sqrt{\frac{1}{n} \sum_{i=1}^{n_x} x_i^2}}  - \\ e^{\frac{1}{n}\sum_{i=1}^{n_x} cos(2\pi x_i))} + 20 + en \\  x \in [-32.786, 32.786]
\end{split}
\]
A rotated version of Ackley, $f_{ack}^R$,  was also investigated. The function was rotated with the rotation matrices for 2,10,30 and 50 dimensions defined in the CEC2005 \cite{suganthan2005problem} benchmark set. 

\textbf{Griewank Function}
\[\begin{split}
f_{gri}(x) = 1 + \frac{1}{4000} \sum_{i=1}^{n_x} x^2_i - 
\prod_{i=1}^{n_x} cos(\frac{x_i}{\sqrt{i}}) \\
 x \in [-600, 600]
\end{split}\]

\textbf{Salomon's Function}
\[\begin{split}
f_{sol}(x) = -cos(2 \pi \sum_{i=1}^{n_x} x^2_i ) + 0.1 \sqrt{\sum_{i=1}^{n_x} x^2_i} + 1 \\
x \in [-100, 100] 
\end{split}\]

\subsection{Quality Measures and emprical analysis}
The fitness of the best solutions over 30 independent runs were measured. The Mann Whitney U test with a p-value of 0.05 is used to compare the best values for different population sizes.  

The populations that are used for the above mentioned statistical test consist of the best fitness at the final iteration for each of the 30 iterations. The test compares the different population sizes pairwise to the best-performing one (the lowest average fitness). In the result tables \textbf{bold} values indicate the best averages and grey cells indicate significant differences (compared to the best population size).  

\section{Results} \label{sec:results}
\subsection{PSO}

\input{includes/PSO-tableD2.tex}
\input{includes/PSO-tableD10.tex}
\input{includes/PSO-tableD30.tex}
\input{includes/PSO-tableD50.tex}

Tables \ref{PSO:D2} to \ref{PSO:D50} show the average and standard deviation of the best fitnesses at the final of the PSO. For most of the problems the best swarm size seemed to be at 35 of 40 particles, but for most cases the differences between the best performing swarm size and some smaller swarms was insignificant (the white blocks).  For the 30 and 50 dimensional problems, the differences between the best performing swarm size and the smallest swarm sizes (10 to 20) was significant for most of the problems, indicating that the optimum swarm size can differ based on dimensions.

The optimum swarm size seems very problem dependent. Based on the larger standard deviations in Table \ref{PSO:D50} the PSO with 50-dimensional problems might need more than 5000 iterations to converge properly.
%\FloatBarrier

\subsection{GA}
\input{includes/GA-tableD2.tex}
\input{includes/GA-tableD10.tex}
\input{includes/GA-tableD30.tex}
\input{includes/GA-tableD50.tex}

Tables \ref{GA:D2} to \ref{GA:D50} shows the average fitness of the best individual in the last generation over 30 independent runs. The optimum population size for the GA is very population dependent with the smallest population performing better than the larger populations on some problems. 

Some observations based on the results:
\begin{itemize}
	\item The four non-rotated separable functions (Absolute, Spherical, Michaelwicz and Rastrigin) consistently performed well with small population sizes, regardless of problem dimensions. 
	\item The Ackley and rotated Ackley functions also have the best performance on smaller populations, with many of differences between population sizes insignificant. 
	\item The optimum population size is problem dependent and does not correlate directly with the number of problem dimensions. 
\end{itemize}
\subsection{EP}
\input{includes/EP-tableD2.tex}
\input{includes/EP-tableD10.tex}
\input{includes/EP-tableD30.tex}
\input{includes/EP-tableD50.tex}

Tables \ref{EP:D2} to \ref{EP:D50} show the best fitness at the final generation of the EP. The problems with 10, 30 and 50 dimensions seems tp perform best on bigger population sizes, but for most of the problems the difference between 160 and 120 individuals is not statistically significant. 

The similarities between the 10, 30 and 50 dimensional problems as well as the lack of significance between performance of the different  population sizes, indicate no obvious correlation between the swarm size and the number of dimensions of the problem. The simple EP that was used in this simulation seems to get worse solutions for the same population sizes as the GA. 

\subsection{DE}
\input{includes/DE-tableD2.tex}
\input{includes/DE-tableD10.tex}
\input{includes/DE-tableD30.tex}
\input{includes/DE-tableD50.tex}
Tables \ref{DE:D2} to \ref{DE:D50} show the results of the DE algorithm. The tables not the average fitness and standard deviation of the best individual in the final generation over multiple independent runs. Grey cells indicate a significant difference between that population size and the best-performing population size. 
The DE algorithm obtained similar results to the GA: The optimum population size varies a lot and a few of the 'best' averages are in the smallest populations. 
Some observations:
\begin{itemize}
	\item The Uni-Modal problems seems less sensitive to population size than the other multi-modal problems. 
\item The Ackley and Griewank functions performed well over different population sizes
	\item The optimum population size is problem dependent. 
\end{itemize}

\section{Conclusion} \label{sec:concl}
As shown in the previous section, the optimum population size for every function is problem dependent but no direct correlation can be drawn between the problem dimensions and the population size. 

The PSO and EP showed some tendencies towards more dimensions requiring a bigger swarm size, but the results were insignificant compared to some of the smaller population sizes. The best population size can also be influenced by the parameters set as discussed in the Implementation section. 

The GA and DE algorithms had best solutions that were more distributed over the population sizes than te other two and there is even less of a pattern visible. 

Overall the optimum population size cannot be directly determined from the number of problem dimensions but is dependent on the problem and the algorithms. 

\bibliography{bibliography}
\bibliographystyle{plain}
\end{document}