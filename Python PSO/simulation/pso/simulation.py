import itertools
import pickle
from ctypes import c_int
from multiprocessing import Value, Lock

from common.settings import iterations, runs, dimensions, problems
from pso.swarm import Swarm, type_name

start_counter = Value(c_int)
complete_counter = Value(c_int)
counter_lock = Lock()

sim_count = 0

swarm_size = (50,60,70)
def init(l, v, st, ec):
    global start_counter, counter_lock, sim_count, complete_counter
    counter_lock = l
    start_counter = v
    sim_count = st
    complete_counter = ec


def increment(counter):
    # global start_counter
    with counter_lock:
        counter.value += 1
        return counter.value


def operation(args):
    c = increment(start_counter)
    _p, _d, _s = args
    print("Simulation #{}: {} D{} S{}".format(c, type_name(_p), _d, _s))
    data = Swarm(_p, _d, _s, iterations)(runs)
    print("Simulation #{} Dumping Results".format(c))
    pickle.dump(data, open("out/D{}/{}.pickle".format(_d, data["name"]), "wb"))
    print("Simulation #{} Complete ({:.2f}%)".format(c, increment(complete_counter) / sim_count * 100))


if __name__ == '__main__':
    from multiprocessing.pool import Pool
    from os import cpu_count, makedirs

    start_counter = Value(c_int)
    counter_lock = Lock()

    simulations = itertools.product(problems, dimensions, swarm_size)
    sim_count = len(problems) * len(dimensions) * len(swarm_size)

    workers = cpu_count() or 1
    print("{} workers".format(workers))
    print("{} simulations @ {} runs each\n".format(sim_count, runs))
    for d in dimensions:
        makedirs("out/D{}".format(d), exist_ok=True)
    global simCounter
    simCounter = itertools.count(1)
    pool = Pool(processes=workers, initializer=init,
                initargs=[counter_lock, start_counter, sim_count, complete_counter])
    result = pool.map(operation, simulations)
    # operation(next(simulations))
