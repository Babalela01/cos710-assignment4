import random


class Particle:
    def __init__(self, dimensions, boundary):
        self.position = tuple(random.uniform(*boundary) for i in range(dimensions))
        self.velocity = tuple([0] * dimensions)
        self.dimensions = dimensions
        self.boundary = boundary
        self.fitness = None
        self.best_fitness = float('inf')
        self.best_position = None

    def is_feasible(self):
        # return reduce(lambda a, v: a and v in self.boundary, self.position)
        feasible, i = True, 0
        while i < self.dimensions and feasible:
            feasible = self.boundary[0] <= self.position[i] <= self.boundary[1]
            i += 1
        return feasible

    def update_best(self):
        if self.fitness < self.best_fitness and self.is_feasible():
            self.best_position = self.position
            self.best_fitness = self.fitness

    def __lt__(self, other):
        return self.fitness < other.fitness

    def __iter__(self):
        return self.position.__iter__()

    def __getitem__(self, item):
        return self.position[item]
