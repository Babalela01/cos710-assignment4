import statistics
from operator import add

from pso.velocity import Velocity

from pso.particle import Particle


def type_name(value):
    try:
        return type(value).__name__ + type_name(value.nested_problem)
    except:
        return type(value).__name__


class Swarm:
    def __init__(self, fitness_func, dimensions=10, swarm_size=30, max_iterations=1000):
        self.particles = None
        self.iteration_index = None
        self.best_position = None
        self.best_fitness = None

        self.boundary = fitness_func.boundary
        self.max_iterations = max_iterations
        self.velocity_func = Velocity()
        self.dimensions = dimensions
        self.fitness_func = fitness_func
        self.swarm_size = swarm_size

    def __call__(self, runs=50):
        problem_name = type_name(self.fitness_func)
        return {
            "boundary": self.boundary,
            "dimensions": self.dimensions,
            "max-iterations": self.max_iterations,
            "nested_problem": problem_name,
            "swarm-size": self.swarm_size,
            "run-count": runs,
            "name": "{}-D{}-S{}".format(problem_name,
                                        self.dimensions,
                                        self.swarm_size),
            "run-data": tuple(self.run() for i in range(runs))
        }

    def run(self):
        self.particles = tuple(Particle(self.dimensions, self.boundary) for i in range(self.swarm_size))
        self.velocity_func.reset(self.dimensions)
        self.best_position = []
        self.best_fitness = float('inf')
        return tuple(self.iteration() for self.iteration_index in range(0, self.max_iterations))

    def iteration(self):

        for p in self.particles:
            p.fitness = self.fitness_func(p.position)
            p.update_best()
            if p.best_fitness < self.best_fitness:
                self.best_position = p.best_position
                self.best_fitness = p.best_fitness

        for p in self.particles:
            p.velocity = self.velocity_func(p, self)
            p.position = tuple(map(add, p.position, p.velocity))

        return {"gbest": self.best_fitness,
                "swarm_best": min(self.particles).fitness,
                "avg_pbest": statistics.mean(v.best_fitness for v in self.particles),
                "avg_fitness": statistics.mean(v.fitness for v in self.particles),
                # "all_best": tuple(v.best_fitness for v in self.particles),
                "all_fitness": tuple(v.fitness for v in self.particles)}
