from random import random


class Velocity:
    def __init__(self, w=0.789244, c1=1.4296180, c2=1.4296180):
        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.cognitive = lambda p: p.best_position
        self.social = lambda swarm: swarm.best_position

        self.r1 = None
        self.r2 = None
        self.dimensions = None

    def __call__(self, p, swarm):
        lx = p.position
        lv = p.velocity
        cog = self.cognitive(p)
        soc = self.social(swarm)
        return tuple(
            (self.w * lv[d] +
             self.c1 * self.r1[d] * (cog[d] - lx[d]) +
             self.c2 * self.r2[d] * (soc[d] - lx[d]))
            for d in range(self.dimensions))

    def reset(self, dimensions):
        self.dimensions = dimensions
        self.r1 = tuple(random() for i in range(dimensions))
        self.r2 = tuple(random() for i in range(dimensions))

    def get_settings(self):
        return {"w": self.w,
                "c1": self.c1,
                "c2": self.c2}
