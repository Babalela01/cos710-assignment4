import statistics

# {
#     "boundary": ...,
#     "dimensions": ...,
#     "max-iterations": ...,
#     "problem": ...,
#     "velocity": ...,
#     "comparator": ...,
#     "swarm-size": ...,
#     "run-count": ...,
#     "name": ...,
#     "run-data": [
#        [ {"gbest": ..., "avg_pbest": ..., "avg_fitness": ..., "swarm_best": ...}, ... ]
#        [ {"gbest": ..., "avg_pbest": ..., "avg_fitness": ..., "swarm_best": ...}, ... ]
#        ....
#      ]
# }

# Accuracy (Fitness of solution)
# Efficiency - Timet taken to reach solution
# Consistency - Deviation over multiple runs
from itertools import product

from scipy.stats import mannwhitneyu

from common.settings import dimensions, problems
from pso.swarm import type_name

extension = ".csv"
problem_names = [type_name(x) for x in problems]
swarm_size = (20, 40, 60, 80, 100, 120, 140, 160)
problem_count = len(problems)
runs = 30


def do_manU(x1, x2):
    muw = mannwhitneyu(x1, x2)
    p = muw[1] * 2
    return p


numfmt = "${:.3f}_{{{:.3f}}}$"

coloredfmt = r"\cellcolor{{gray25}}" + numfmt
boldfmt = r"\boldmath" + numfmt


def fmt(arr):
    best = round(min(a for (a, d, s) in arr), 3)
    return (element(best, x) for x in arr)


def element(maxs, x):
    return (boldfmt if round(x[0], 3) == maxs else coloredfmt if x[2] else numfmt).format(*x)


def mannu(values):
    mins = float('inf')
    minpop = None
    for run in values:
        if run['avg'] < mins:
            mins = run['avg']
            minpop = run['pop']

    for run in values:
        run['sig'] = (do_manU(minpop, run['pop']) <= 0.05) if run['avg'] != mins else False

    return [(run['avg'], run['dev'], run['sig']) for run in values]


def get_last(i, inpath):
    with open(inpath.format(i=i)) as f:
        return float(f.readlines()[-1].strip())


path = "../../java/Experiments/Study_D{d}/data/{alg}_D{d}_S{s}/{p}/FUN{{i}}.tsv"


# Fitness and  std dev at last generation
def main(algo, dimension):
    settings = {
        "d": dimension,
        "alg": algo
    }
    table = {}
    for p in problems:
        prob = type_name(p)
        settings["p"] = prob
        values = []
        for swarm in swarm_size:
            settings["s"] = swarm
            in_path = path.format(**settings)
            print("Handling path: " + in_path)
            bests = [get_last(i, in_path) for i in range(runs)]
            values.append({"avg": statistics.mean(bests), "dev": statistics.stdev(bests), "pop": bests})
        values = mannu(values)
        table[prob] = values

    headings = [str(s) + " individuals" for s in swarm_size]
    outfile_name = "../../report/includes/{}-tableD{}.tex".format(algo, dimension)
    tablebegin = r"\begin{table*}" + "\n" + \
                 r"\caption{" + algo + " with " + str(
        dimension) + " dimensions: Average and standard deviation at final iteration}\n" + \
                 "\label{" + algo + ":D" + str(dimension) + "}" \
                 r"\begin{tabular}{l|" + (" r" * len(swarm_size)) + "}\n" + \
                 " & " + "&".join(headings) + r"\\ \hline" + "\n"

    lines_arr = [p.fname() + "&" + "&".join(fmt(table[type_name(p)])) + r"\\" for p in problems]
    lines_arr.append("\hline")
    lines_arr.insert(5, "\hline")
    lines_arr.insert(2, "\hline")
    lines = "\n".join(lines_arr)
    tableend = "\n" + r"\end{tabular}\end{table*}"

    with open(outfile_name, "w") as outfile:
        outfile.write(tablebegin)
        outfile.write(lines)
        outfile.write(tableend)
    return True


#
# def best_graphs(data_iterations, title, file_, normal_data):
#     features = [("gbest", "Global best", "--"), ("avg_pbest", "Average Personal best", ":"),
#                 ("avg_fitness", "Average Fitness", '-')]
#     graph = [{"name": t[1],
#               "data": [statistics.mean(build_row(t[0], r))
#                        for r in data_iterations],
#               "settings": t[2]} for t in features]
#     # iterations_graph(len(data_iterations), graph, title, file_, graph_maxit, normal_data)


if __name__ == '__main__':
    print("Starting")
    from multiprocessing.pool import Pool

    with Pool(1) as pool:
        print('Pool')
        algorithms = ["GA", "EP", "DE"]
        result = pool.starmap(main, product(algorithms, dimensions))
