def rotate(x, transform):
    xlen = len(x)
    result = [0.0] * xlen
    for i in range(xlen):
        for j in range(xlen):
            result[i] += x[j] * transform[j][i]
    return result
