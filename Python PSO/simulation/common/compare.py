class Min:
    @staticmethod
    def compare(val1, val2):
        return val2 is None or val1 <= val2

