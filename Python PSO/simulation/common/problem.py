import math
import operator
from abc import abstractmethod
from functools import reduce

from common.rotate import rotate


class Problem:
    @abstractmethod
    def __init__(self, boundary):
        self.boundary = boundary

    @abstractmethod
    def __call__(self, particle):
        pass

    @abstractmethod
    def fname(self):
        pass


# Uni-modal [-5.12, 5.12]
class Spherical(Problem):
    def fname(self):
        return "$f_{sphe}$"

    def __init__(self, boundary=(-5.12, 5.12)):
        super().__init__(boundary)

    def __call__(self, position):
        return sum(x ** 2 for x in position)


# Uni-modal [-100, 100]
class Absolute(Problem):
    def fname(self):
        return "$f_{abs}$"

    def __init__(self, boundary=(-100, 100)):
        super().__init__(boundary)

    def __call__(self, position):
        return sum(abs(v) for v in position)


# Schwefel 1.2 Uni-modal, non-separable (otated hyper-ellipsoids)
class Schwefel12(Problem):
    def __call__(self, position):
        return sum(sum(position[:i + 1]) ** 2 for i in range(len(position)))

    def __init__(self, boundary=(-100, 100)):
        super().__init__(boundary)


# Multi modal [-32.768, 32.768], non-separable
class Ackley(Problem):
    def fname(self):
        return "$f_{ack}$"

    def __init__(self, boundary=(-32.768, 32.768)):
        super().__init__(boundary)

    def __call__(self, position):
        lx = position
        a = 20.0
        b = 0.2
        c = 2.0 * math.pi
        d = len(lx)

        sum_square = sum(x ** 2 for x in lx)
        sum_cos = sum(math.cos(c * x) for x in lx)

        term1 = (-a * math.exp(-b * math.sqrt(sum_square / d)))
        term2 = -math.exp(sum_cos / d)
        return a + math.exp(1) + term1 + term2


# Multi modal [-100, 100]
class Salomon(Problem):
    def fname(self):
        return "$f_{sal}$"

    def __call__(self, position):
        lx = position

        sum_square = sum(x ** 2 for x in lx)

        sqrt = math.sqrt(sum_square)
        term1 = -math.cos(2 * math.pi * sqrt)
        term2 = 0.1 * sqrt
        return term1 + term2 + 1

    def __init__(self, boundary=(-100, 100)):
        super().__init__(boundary)


# Multi modal [-600, 600]
class Griewank(Problem):
    def fname(self):
        return "$f_{grie}$"

    def __call__(self, position):
        lx = position
        sumsq = sum(x ** 2 for x in lx)
        prod = reduce(operator.mul, (math.cos(x / math.sqrt(i + 1)) for i, x in enumerate(lx)), 1)

        return 1 + sumsq / 4000.0 - prod

    def __init__(self, boundary=(-600, 600)):
        super().__init__(boundary)


# Multi modal [-5.12, 5.12]
class Rastrigin(Problem):
    def fname(self):
        return "$f_{ras}$"

    def __call__(self, position):
        lx = position

        tmp = sum(x * x - 10.0 * math.cos(2.0 * math.pi * x) for x in lx)
        return 10 * len(lx) + tmp

    def __init__(self, boundary=(-5.12, 5.12)):
        super().__init__(boundary)


class Michalewicz(Problem):
    def fname(self):
        return "$f_{mic}$"

    def __call__(self, position):
        m = self.m
        lx = position
        sin = math.sin
        return -sum(sin(x) * sin(j * x ** 2 / math.pi) ** (2 * m) for j, x in enumerate(lx))

    def __init__(self, boundary=(0, math.pi), m=10):
        super().__init__(boundary)
        self.m = m


class Rotated(Problem):
    def fname(self):
        return self.nested_problem.fname()[:-1] + "^R$"


    def __init__(self, problem, matrices):
        super().__init__(problem.boundary)

        self.matrices = matrices
        self.nested_problem = problem

    def __call__(self, position):
        lx = position
        dim = len(lx)
        transform = self.matrices[dim]
        return self.nested_problem(rotate(lx, transform))
