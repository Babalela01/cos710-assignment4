def read(fname, d):
    pattern = "data/{}_M_D{}.txt".format(fname,d)
    with open(pattern) as file:
        lines = file.readlines()
    return tuple(map(lambda s: [*map(float, s.strip().split())], lines))


def read_all(fname, darr):
    return {d : read(fname, d) for d in darr}