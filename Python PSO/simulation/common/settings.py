from common.problem import Absolute, Spherical, Michalewicz, Rastrigin, Rotated, Ackley, Salomon, Griewank

from common.readData import read_all

runs = 30
dimensions = (2, 10, 30, 50)
swarm_size = (10, 15, 20, 25, 30, 35, 40)
problems = [Absolute(), Spherical(),  # Schwefel12(),
            Michalewicz(), Rastrigin(), Rotated(Rastrigin(), read_all("rastrigin", dimensions)),
            Ackley(), Rotated(Ackley(), read_all("ackley", dimensions)), Salomon(), Griewank()
            ]
iterations = 5000