//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ep;

import org.uma.jmetal.algorithm.impl.AbstractEvolutionaryAlgorithm;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.solution.impl.DefaultDoubleSolution;
import org.uma.jmetal.util.SolutionListUtils;
import org.uma.jmetal.util.comparator.ObjectiveComparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class implementing a (mu + lambda) Evolution Strategy (lambda must be divisible by mu)
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
@SuppressWarnings("serial")
public class EP extends AbstractEvolutionaryAlgorithm<DoubleSolution, List<DoubleSolution>> {

    private final int np;
    private final Comparator<EpSolution> comparator = (x, y) -> Double.compare(y.getScore(), x.getScore());
    private final Comparator<DoubleSolution> objectiveComparator = new ObjectiveComparator<>(0);
    private final ArrayList<DoubleSolution> results;
    private int maxEvaluations;
    private int evaluations;
    private MutationOperator<DoubleSolution> mutation;
    private SelectionOperator<List<DoubleSolution>, DoubleSolution> selectionOperator = new BinaryTournamentSelection<>();

    /**
     * Constructor
     */
    public EP(Problem<DoubleSolution> problem, int maxEvaluations, int populationSize,
              MutationOperator<DoubleSolution> mutation, int np) {
        this.setProblem(problem);
        this.maxEvaluations = maxEvaluations;
        this.mutation = mutation;
        this.setMaxPopulationSize(populationSize);
        this.np = np;
        this.results = new ArrayList<>(maxEvaluations);
    }

    @Override
    protected void initProgress() {
        evaluations = 1;
    }

    @Override
    protected void updateProgress() {
        List<DoubleSolution> population = getPopulation();

        DoubleSolution doubleSolution = SolutionListUtils.findBestSolution(population,
                objectiveComparator);
        results.add(doubleSolution);
        evaluations += 1;
    }

    @Override
    protected boolean isStoppingConditionReached() {
        return evaluations >= maxEvaluations;
    }

    @Override
    protected List<DoubleSolution> createInitialPopulation() {
        int maxPopulationSize = getMaxPopulationSize();
        List<DoubleSolution> population = new ArrayList<>(maxPopulationSize);
        DoubleProblem problem = (DoubleProblem) getProblem();
        for (int i = 0; i < maxPopulationSize; i++) {
            population.add(new EpSolution(problem));
        }

        return population;
    }

    @Override
    protected List<DoubleSolution> evaluatePopulation(List<DoubleSolution> population) {
        for (DoubleSolution solution : population) {
            getProblem().evaluate(solution);
        }

        return population;
    }

    @Override
    protected List<DoubleSolution> selection(List<DoubleSolution> population) {
        return population;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected List<DoubleSolution> reproduction(List<DoubleSolution> population) {
        return population.stream().map(s -> mutation.execute(s.copy())).collect(Collectors.toList());
    }

    @Override
    protected List<DoubleSolution> replacement(List<DoubleSolution> populationO,
                                               List<DoubleSolution> offspringPopulation) {
        offspringPopulation.addAll(populationO);

        List<EpSolution> population = offspringPopulation.stream().map(x -> (EpSolution) x).collect(Collectors.toList());
        for (EpSolution solution : population) {
            List<EpSolution> competition = SolutionListUtils.selectNRandomDifferentSolutions(np, population);
            int score = 0;
            double origFitness = solution.getObjective(0);
            for (EpSolution comp : competition) {
                double compFitness = comp.getObjective(0);
                if (origFitness < compFitness) {
                    score++;
                }
            }
            solution.setScore(score);
        }

        population.sort(comparator);

        return population.subList(0, getMaxPopulationSize()).stream().collect(Collectors.toList());
    }

    @Override
    public List<DoubleSolution> getResult() {
        return results;
    }

    @Override
    public String getName() {
        return "EP";
    }

    @Override
    public String getDescription() {
        return "Evolutionary Program";
    }

    private static class EpSolution extends DefaultDoubleSolution {
        public int getScore() {
            return score;
        }

        private int score;

        public EpSolution(DoubleProblem problem) {
            super(problem);
        }

        public EpSolution(DefaultDoubleSolution solution) {
            super(solution);
        }

        public EpSolution(DoubleSolution solution) {
            super((DefaultDoubleSolution) solution);
        }


        public void setScore(int score) {
            this.score = score;
        }

        @Override
        public EpSolution copy() {
            return new EpSolution(this);
        }
    }
}
