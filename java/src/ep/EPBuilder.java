//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package ep;

import ep.EP;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.AlgorithmBuilder;

import java.util.List;

/**
 * Class implementing a (mu , lambda) Evolution Strategy (lambda must be divisible by mu)
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public class EPBuilder implements AlgorithmBuilder<Algorithm<List<DoubleSolution>>> {
    public EPBuilder setNp(int np) {
        this.np = np;
        return this;
    }

    private int np;
    private int populationSize;

    private Problem<DoubleSolution> problem;
    private int maxGenerators;
    private MutationOperator<DoubleSolution> mutation;


    public EPBuilder(Problem<DoubleSolution> problem, MutationOperator<DoubleSolution> mutationOperator) {
        this.problem = problem;
        this.populationSize = 100;
        this.maxGenerators = 5000;
        this.mutation = mutationOperator;
        this.np = 10;
    }

    public EPBuilder setMaxGenerators(int maxGenerators) {
        this.maxGenerators = maxGenerators;

        return this;
    }

    public EPBuilder setPopulationSize(int maxEvaluations) {
        this.populationSize = maxEvaluations;

        return this;
    }


    @Override
    public Algorithm<List<DoubleSolution>> build() {
        return new EP(problem, maxGenerators, populationSize, mutation, np);
    }

  /* Getters */
}
