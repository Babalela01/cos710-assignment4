package operators;

import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.solution.DoubleSolution;

import java.util.Random;

/**
 * @author renet
 *         on 2016/05/24.
 */
public class GaussianMutation implements MutationOperator<DoubleSolution> {

    private final Random randomGenenerator;
    private final double probability;
    private final double perturbation;

    public GaussianMutation(double mutationRate, double perturbation) {
        randomGenenerator = new Random();
        probability = mutationRate;
        this.perturbation = perturbation;
    }

    @Override
    public DoubleSolution execute(DoubleSolution solution) {
        for (int i = 0; i < solution.getNumberOfVariables(); i++) {
            if (randomGenenerator.nextDouble() < probability) {

                double tmp = solution.getVariableValue(i) + randomGenenerator.nextGaussian() * perturbation;

                if (tmp < solution.getLowerBound(i)) {
                    tmp = solution.getLowerBound(i);
                } else if (tmp > solution.getUpperBound(i)) {
                    tmp = solution.getUpperBound(i);
                }

                solution.setVariableValue(i, tmp);
            }
        }
        return solution;
    }
}
