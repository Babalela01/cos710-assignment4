package operators;

import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.solution.DoubleSolution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author renet
 *         on 2016/05/24.
 */
public class ArithmeticCrossover implements CrossoverOperator<DoubleSolution> {

    final Random random = new Random();
    private final double probability;

    public ArithmeticCrossover(double probability) {
        this.probability = probability;
    }

    @Override
    public int getNumberOfParents() {
        return 2;
    }

    @Override
    public List<DoubleSolution> execute(List<DoubleSolution> solutions) {
        return doCrossover(solutions.get(0), solutions.get(1)) ;
    }

    private List<DoubleSolution> doCrossover(DoubleSolution parent1, DoubleSolution parent2) {
        DoubleSolution offspring1 = (DoubleSolution) parent1.copy();
        DoubleSolution offspring2 = (DoubleSolution) parent2.copy();

        if (random.nextDouble() < probability) {
            double y = random.nextDouble();
            for (int i=0; i<parent1.getNumberOfVariables(); ++i) {
                double p1 = parent1.getVariableValue(i);
                double p2 = parent2.getVariableValue(i);
                offspring1.setVariableValue(i, y * p1 + (1-y) * p2);
                offspring2.setVariableValue(i, (1-y) * p1 + y * p2);
            }
        }
        return Arrays.asList(offspring1, offspring2);
    }
}
