//
// Special Session on Real-Parameter Optimization at CEC-05
// Edinburgh, UK, 2-5 Sept. 2005
//
// Organizers:
//	Prof. Kalyanmoy Deb
//		deb@iitk.ac.in
//		http://www.iitk.ac.in/kangal/deb.htm
//	A/Prof. P. N. Suganthan
//		epnsugan@ntu.edu.sg
//		http://www.ntu.edu.sg/home/EPNSugan
//
// Java version of the org.uma.test functions
//
// Matlab reference code
//	http://www.ntu.edu.sg/home/EPNSugan
//
// Java version developer:
//	Assistant Prof. Ying-ping Chen
//		Department of Computer Science
//		National Chiao Tung University
//		HsinChu City, Taiwan
//		ypchen@csie.nctu.edu.tw
//		http://www.csie.nctu.edu.tw/~ypchen/
//
// Typical use of the org.uma.test functions in the Benchmark:
//
//		// Create a Benchmark object
// 		Benchmark theBenchmark = new Benchmark();
//		// Use the factory function call to create a org.uma.test function object
//		//		org.uma.test function 3 with 50 dimension
//		//		the object class is "TestFunc"
//		TestFunc aTestFunc = theBenchmark.testFunctionFactory(3, 50);
//		// Invoke the function with x
//		double experimentoutput = aTestFunc.f(x);
//
// Version 0.90
//		Currently, this version cannot handle any numbers of dimensions.
//		It cannot generate the shifted global optima and rotation matrices
//		that are not provided with the Matlab reference code.
//		It can handle all cases whose data files are provided with
//		the Matlab reference code.
// Version 0.91
//		Revised according to the Matlab reference code and the PDF document
//		dated March 8, 2005.
//
package problem;

import org.uma.jmetal.problem.impl.AbstractDoubleProblem;
import org.uma.jmetal.problem.singleobjective.cec2005competitioncode.Benchmark;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.JMetalException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class RotatedAckley extends AbstractDoubleProblem {

    // Fixed (class) parameters
    static final public String FUNCTION_NAME =
            "Rotated Ackley's Function with Global Optimum on Bounds";

    static final public String DEFAULT_FILE_MX_PREFIX = "resources/ackley_M_D";
    static final public String DEFAULT_FILE_MX_SUFFIX = ".txt";

    // Shifted global optimum
    private final double[][] m_matrix;

    // Constructors
    public RotatedAckley(int dimension) throws Exception {
        this(dimension,
                DEFAULT_FILE_MX_PREFIX + dimension + DEFAULT_FILE_MX_SUFFIX);
    }

    public RotatedAckley(int dimensions, String file_m)
            throws Exception {
        setNumberOfVariables(dimensions);
        setNumberOfObjectives(1);
        setName("RotatedAckley");

        List<Double> lowerLimit = new ArrayList<>(getNumberOfVariables()) ;
        List<Double> upperLimit = new ArrayList<>(getNumberOfVariables()) ;

        for (int i = 0; i < getNumberOfVariables(); i++) {
            lowerLimit.add(-32.768);
            upperLimit.add(32.768);
        }

        setLowerLimit(lowerLimit);
        setUpperLimit(upperLimit);

        // Note: dimensions starts from 0
        m_matrix = new double[dimensions][dimensions];
        // Load the matrix
        BufferedReader brSrc =
                new BufferedReader(
                        new InputStreamReader(new FileInputStream(file_m))) ;
        //BufferedReader brSrc = new BufferedReader(new FileReader(file));
        Benchmark.loadMatrix(brSrc, dimensions, dimensions, m_matrix);
        brSrc.close();
        //Benchmark.loadMatrixFromFile(file_m, dimensions, dimensions, m_matrix);
    }

    @Override
    public void evaluate(DoubleSolution solution) {
        int dimensions = getNumberOfVariables() ;
        double[] x = IntStream.range(0, dimensions)
                .mapToDouble(solution::getVariableValue).toArray();

        double[] m_zM = new double[dimensions];
        Benchmark.rotate(m_zM, x, m_matrix);
        double result = Benchmark.ackley(m_zM);

        solution.setObjective(0, result);
    }
}
