package problem;

import org.uma.jmetal.problem.impl.AbstractDoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * @author renet
 *         on 2016/05/23.
 */
public class Salomon extends AbstractDoubleProblem {
    public Salomon(Integer numberOfVariables) {
        setNumberOfVariables(numberOfVariables);
        setNumberOfObjectives(1);
        setName("Salomon");

        List<Double> lowerLimit = new ArrayList<>(getNumberOfVariables()) ;
        List<Double> upperLimit = new ArrayList<>(getNumberOfVariables()) ;

        for (int i = 0; i < getNumberOfVariables(); i++) {
            lowerLimit.add(-100.0);
            upperLimit.add(100.0);
        }

        setLowerLimit(lowerLimit);
        setUpperLimit(upperLimit);
    }

    /** Evaluate() method */
    @Override
    public void evaluate(DoubleSolution solution) {
        int numberOfVariables = getNumberOfVariables() ;

        /*double[] x = new double[numberOfVariables] ;

        for (int i = 0; i < numberOfVariables; i++) {
            x[i] = solution.getVariableValue(i) ;
        }*/

        double[] x = IntStream.range(0, numberOfVariables)
                .mapToDouble(solution::getVariableValue).toArray();
        double sum_square = DoubleStream.of(x).map(d -> d*d).sum();
        double sqrt = Math.sqrt(sum_square);
        double term1 = -Math.cos(2* Math.PI * sqrt);
        double term2 = 0.1 * sqrt;

        double result = term1 + term2 + 1;
        solution.setObjective(0, result);
    }
}
