package problem;

import org.uma.jmetal.problem.impl.AbstractDoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author renet
 *         on 2016/05/23.
 */
public class Michalewicz extends AbstractDoubleProblem {
    public Michalewicz(Integer numberOfVariables) {
        setNumberOfVariables(numberOfVariables);
        setNumberOfObjectives(1);
        setName("Michalewicz");

        List<Double> lowerLimit = new ArrayList<>(getNumberOfVariables()) ;
        List<Double> upperLimit = new ArrayList<>(getNumberOfVariables()) ;

        for (int i = 0; i < getNumberOfVariables(); i++) {
            lowerLimit.add(0.0);
            upperLimit.add(Math.PI);
        }

        setLowerLimit(lowerLimit);
        setUpperLimit(upperLimit);
    }

    /** Evaluate() method */
    @Override
    public void evaluate(DoubleSolution solution) {
        int numberOfVariables = getNumberOfVariables() ;

        /*double[] x = new double[numberOfVariables] ;

        for (int i = 0; i < numberOfVariables; i++) {
            x[i] = solution.getVariableValue(i) ;
        }*/
        double m = 10;
        double[] lx = IntStream.range(0, numberOfVariables)
                .mapToDouble(solution::getVariableValue).toArray();

        double result = -IntStream.range(0,numberOfVariables).mapToDouble(i -> {
            double x = lx[i];

            return Math.sin(x) * Math.pow(Math.sin(i*x*x/Math.PI), 2*m);
        }).sum();
        solution.setObjective(0, result);
    }
}
