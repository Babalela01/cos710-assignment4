//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ep.EPBuilder;
import operators.GaussianMutation;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.singleobjective.differentialevolution.DifferentialEvolutionBuilder;
import org.uma.jmetal.algorithm.singleobjective.geneticalgorithm.GeneticAlgorithmBuilder;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.experiment.Experiment;
import org.uma.jmetal.util.experiment.ExperimentBuilder;
import org.uma.jmetal.util.experiment.component.ExecuteAlgorithms;
import org.uma.jmetal.util.experiment.util.TaggedAlgorithm;
import problem.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Example of experimental study based on solving the ZDT problems with four versions of NSGA-II, each
 * of them applying a different crossover probability (from 0.7 to 1.0).
 * <p>
 * This experiment assumes that the reference Pareto front are not known, so the names of files containing
 * them and the directory where they are located must be specified.
 * <p>
 * Six quality indicators are used for performance assessment.
 * <p>
 * The steps to carry out the experiment are:
 * 1. Configure the experiment
 * 2. Execute the algorithms
 * 3. Generate the reference Pareto fronts
 * 4. Compute the quality indicators
 * 5. Generate Latex tables reporting means and medians
 * 6. Generate Latex tables with the result of applying the Wilcoxon Rank Sum Test
 * 7. Generate Latex tables with the ranking obtained by applying the Friedman test
 * 8. Generate R scripts to obtain boxplots
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public class Study {
    private static final int INDEPENDENT_RUNS = 30;
    private static final int MAX_GENERATIONS = 5000;

    private static int[] dimensions = new int[]{2, 10,30,50};
    private static int[] population = new int[]{20, 40, 60, 80, 100, 120, 140, 160};


    public static void main(String[] args) throws Exception {
        int numberOfCores = Integer.parseInt(args[0]);
        System.out.println("Starting");
        for (int d : dimensions) {
            System.out.println("Dimensions " + d);
            List<DoubleProblem> problemList = getProblems(d);

            String experimentBaseDirectory = "Experiments";
            String referenceFrontDirectory = "Experiments/rf";

            List<TaggedAlgorithm<List<DoubleSolution>>> algorithmList = configureAlgorithmList(problemList, INDEPENDENT_RUNS);
            Experiment<DoubleSolution, List<DoubleSolution>> experiment =
                    new ExperimentBuilder<DoubleSolution, List<DoubleSolution>>("Study_D" + d)
                            .setAlgorithmList(algorithmList)
                            .setProblemList(problemList.stream().collect(Collectors.toList()))
                            .setExperimentBaseDirectory(experimentBaseDirectory)
                            .setOutputParetoFrontFileName("FUN")
                            .setOutputParetoSetFileName("VAR")
                            .setReferenceFrontDirectory(referenceFrontDirectory)
                            /*.setIndicatorList(Collections.emptyList())*/
                            .setIndependentRuns(INDEPENDENT_RUNS)
                            .setNumberOfCores(numberOfCores)
                            .build();
            System.out.println("Dimensions " + d + " Running ");
            new ExecuteAlgorithms<>(experiment).run();
//
            /* new GenerateReferenceParetoSetAndFrontFromDoubleSolutions(experiment).run();
             new ComputeQualityIndicators<>(experiment).run();
            new GenerateLatexTablesWithStatistics(experiment).run();
            new GenerateFriedmanTestTables<>(experiment).run();
            new GenerateWilcoxonTestTablesWithR<>(experiment).run();
            new GenerateBoxplotsWithR<>(experiment).setRows(3).setColumns(3).run(); //*/
        }
    }


    private static List<DoubleProblem> getProblems(int d) throws Exception {
        return Arrays.asList(
                new Abs(d),
                new Ackley(d),
                new Griewank(d),
                new Michalewicz(d),
                new Rastrigin(d),
                new RotatedAckley(d),
                new RotatedRastrigin(d),
                new Salomon(d),
                new Schwefel12(d),
                new Sphere(d)
        );
    }

    static List<TaggedAlgorithm<List<DoubleSolution>>> configureAlgorithmList(
            List<DoubleProblem> problemList,
            int independentRuns) {
        int initialCapacity = independentRuns * problemList.size() * population.length;
        List<TaggedAlgorithm<List<DoubleSolution>>> ga = new ArrayList<>(initialCapacity);
        List<TaggedAlgorithm<List<DoubleSolution>>> de = new ArrayList<>(initialCapacity);
        List<TaggedAlgorithm<List<DoubleSolution>>> ep = new ArrayList<>(initialCapacity);


        for (DoubleProblem problem : problemList) {
            for (int popSize : population) {
                for (int run = 0; run < independentRuns; run++) {
                    ga.add(getGA(problem, popSize, run));
                    de.add(getDE(problem, popSize, run));
                    ep.add(getEP(problem, popSize, run));
                }
            }
        }

        List<TaggedAlgorithm<List<DoubleSolution>>> algorithms = new ArrayList<>(initialCapacity);
        algorithms.addAll(de);
       /* algorithms.addAll(ga);*/
        algorithms.addAll(ep);
        return algorithms;
    }

    private static TaggedAlgorithm<List<DoubleSolution>> getEP(DoubleProblem problem, int popSize, int run) {
        Algorithm<List<DoubleSolution>> algorithm;
        algorithm = (new EPBuilder(problem, new GaussianMutation(0.8,0.5)))
                .setMaxGenerators(MAX_GENERATIONS)
                .setNp(popSize/3)
                .setPopulationSize(popSize)
                .build();

        //algorithm.run();
        /*DoubleSolution solution;
        solution = algorithm.getResult();*/
        return new TaggedAlgorithm<>(algorithm, "EP_D" + problem.getNumberOfVariables() + "_S" + popSize, problem, run);
    }

    private static TaggedAlgorithm<List<DoubleSolution>> getDE(DoubleProblem problem, int popSize, int run) {
        Algorithm<List<DoubleSolution>> algorithm;
        algorithm = (new DifferentialEvolutionBuilder(problem))
                .setMaxEvaluations(MAX_GENERATIONS*popSize)
                .setPopulationSize(popSize)
                .build();

        //algorithm.run();
        /*DoubleSolution solution;
        solution = algorithm.getResult();*/
        return new TaggedAlgorithm<>(algorithm, "DE_D" + problem.getNumberOfVariables() + "_S" + popSize, problem, run);
    }

    private static TaggedAlgorithm<List<DoubleSolution>> getGA(DoubleProblem problem, int popSize, int run) {
        Algorithm<List<DoubleSolution>> algorithm = new GeneticAlgorithmBuilder<>(
                problem,
                new operators.ArithmeticCrossover(0.9),
                new operators.GaussianMutation(1.0 / problem.getNumberOfVariables(), 1.0))
                .setPopulationSize(popSize)
                .setMaxEvaluations(MAX_GENERATIONS*popSize)
                .build();
        return new TaggedAlgorithm<>(algorithm, "GA_D" + problem.getNumberOfVariables() + "_S" + popSize, problem, run);
    }
}